package com.redmedia.edayph.events;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.redmedia.edayph.R;
import com.redmedia.edayph.Settings;

public class EventsMapScreen extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_event_map);

	GoogleMap gmap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
	String title = getIntent().getStringExtra("title");
	String fromAndTo = getIntent().getStringExtra("from") + " - " + getIntent().getStringExtra("to");
	String time = ", " + (getIntent().getStringExtra("time").equals("") ? "Time Unavailable" : getIntent().getStringExtra("time"));
	float latitude = getIntent().getFloatArrayExtra("latitude")[0];
	float longitude = getIntent().getFloatArrayExtra("latitude")[1];
	gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));

	MarkerOptions markerOptions = new MarkerOptions();
	markerOptions.position(new LatLng(latitude, longitude));
	markerOptions.title(title);
	markerOptions.snippet(fromAndTo + time);
	gmap.addMarker(markerOptions);

	if (!Settings.isNetworkAvailable(getApplicationContext()))
	    showDialog(0);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
	Builder builder = new AlertDialog.Builder(this);
	builder.setIcon(android.R.drawable.ic_dialog_info);
	builder.setMessage(R.string.event_no_network_prompt_map);
	builder.setTitle("No Network Connection");
	builder.create().show();
	return super.onCreateDialog(id);
    }
}
