package com.redmedia.edayph.events;

import java.io.Serializable;

/**
 * Model for an event. What else?
 * 
 * @author MrUseL3tter
 */
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String title;
    private final String from;
    private final String to;
    private final String time;
    private final String location;
    private final float latitude;
    private final float longitude;

    public Event(String title, String from, String to, String time, String location, float latitude, float longitude) {
	this.title = title;
	this.from = from;
	this.to = to;
	this.time = time;
	this.location = location;
	this.latitude = latitude;
	this.longitude = longitude;
    }

    /**
     * @return the from
     */
    public String getFrom() {
	return from;
    }

    /**
     * @return the latitude
     */
    public float getLatitude() {
	return latitude;
    }

    /**
     * @return the location
     */
    public String getLocation() {
	return location;
    }

    /**
     * @return the longitude
     */
    public float getLongitude() {
	return longitude;
    }

    /**
     * @return the time
     */
    public String getTime() {
	return time;
    }

    /**
     * @return the title
     */
    public String getTitle() {
	return title;
    }

    /**
     * @return the to
     */
    public String getTo() {
	return to;
    }

    public boolean hasMapLocation() {
	return latitude != 0f && longitude != 0f;
    }
}
