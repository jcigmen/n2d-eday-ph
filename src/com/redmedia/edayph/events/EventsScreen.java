package com.redmedia.edayph.events;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.redmedia.edayph.Fonts;
import com.redmedia.edayph.R;
import com.redmedia.edayph.Settings;

public class EventsScreen extends ListActivity implements OnItemClickListener {

    private static final int NO_NETWORK_CONNECTION = 1;
    private static final int ERROR_SYNCING = 2;
    private static final String EVENTS_JSON_URL = "http://spreadsheets.google.com/feeds/list/0Am2Y7_nRMjLsdEJBSGZvX1pYdkxpYkE3c05nMVhEZ1E/od6/public/values?alt=json";

    public static ArrayList<Event> events;

    private class JSONFetcher extends AsyncTask<Void, Integer, String> {

	private ProgressDialog progressDialog;

	@Override
	protected String doInBackground(Void... params) {
	    HttpGet get = new HttpGet(EVENTS_JSON_URL);
	    try {
		HttpResponse response = client.execute(get);
		int status = response.getStatusLine().getStatusCode();
		if (status == 200)
		    return EntityUtils.toString(response.getEntity());
	    } catch (ClientProtocolException e) {
	    } catch (IOException e) {
	    }
	    return null;
	}

	@Override
	protected void onPostExecute(String result) {
	    progressDialog.dismiss();
	    // there was a result
	    if (result != null) {
		processJSON(result);
		populateListView();
		Settings.eventsData = result;
	    } else
		EventsScreen.this.showDialog(ERROR_SYNCING);
	}

	@Override
	protected void onPreExecute() {
	    progressDialog = ProgressDialog.show(EventsScreen.this, "Sync", "Syncing Events...", true, false);
	}

    }

    private HttpClient client = new DefaultHttpClient();

    private Intent eventMapScreenIntent;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	MenuInflater inflater = getMenuInflater();
	inflater.inflate(R.menu.menu_events, menu);
	return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	// for EventMapScreen
	if (events.get(position).hasMapLocation()) {
	    if (eventMapScreenIntent == null)
		eventMapScreenIntent = new Intent(getApplicationContext(), EventsMapScreen.class);
	    else {
		eventMapScreenIntent.removeExtra("title");
		eventMapScreenIntent.removeExtra("to");
		eventMapScreenIntent.removeExtra("from");
		eventMapScreenIntent.removeExtra("time");
		eventMapScreenIntent.removeExtra("latitude");
	    }
	    eventMapScreenIntent.putExtra("title", events.get(position).getTitle());
	    eventMapScreenIntent.putExtra("to", events.get(position).getTo());
	    eventMapScreenIntent.putExtra("from", events.get(position).getFrom());
	    eventMapScreenIntent.putExtra("time", events.get(position).getTime());
	    eventMapScreenIntent.putExtra("latitude", new float[] { events.get(position).getLatitude(), events.get(position).getLongitude() });
	    startActivity(eventMapScreenIntent);
	}
	//for EventDetailsScreen 
	else {
	    Intent intent = new Intent(getApplicationContext(), EventDetailsScreen.class);
	    intent.putExtra("event", events.get(position));
	    startActivity(intent);
	}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	    case R.id.sync:
		fetchData();
	    default:
		break;
	}
	return true;
    }

    private void fetchData() {
	if (Settings.isNetworkAvailable(getApplicationContext()))
	    new JSONFetcher().execute();
	else
	    showDialog(NO_NETWORK_CONNECTION);
    }

    private void populateListView() {
	ArrayList<Map<String, String>> eventsForList = new ArrayList<Map<String, String>>();
	for (int i = 0; i < events.size(); i++) {
	    HashMap<String, String> map = new HashMap<String, String>();
	    map.put("Title", events.get(i).getTitle());
	    map.put("Details", events.get(i).getLocation());
	    eventsForList.add(map);
	}

	String[] from = { "Title", "Details" };
	int[] to = { R.id.eventTitle, R.id.eventDetails };

	SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), eventsForList, R.layout.screen_event_list_texts, from, to);
	adapter.setViewBinder(new EDayViewBinder());
	setListAdapter(adapter);
	getListView().setOnItemClickListener(this);
    }

    private void processJSON(String raw) {
	events = new ArrayList<Event>();
	JSONObject parent = (JSONObject) JSONValue.parse(raw);
	JSONObject feed = (JSONObject) parent.get("feed");
	JSONArray entries = (JSONArray) feed.get("entry");
	for (int i = 0; i < entries.size(); i++) {
	    JSONObject eventEntry = (JSONObject) entries.get(i);
	    String title = ((JSONObject) eventEntry.get("gsx$eventtitle")).get("$t").toString();
	    String from = ((JSONObject) eventEntry.get("gsx$datefrom")).get("$t").toString();
	    String to = ((JSONObject) eventEntry.get("gsx$dateto")).get("$t").toString();
	    String time = ((JSONObject) eventEntry.get("gsx$time")).get("$t").toString();
	    String location = ((JSONObject) eventEntry.get("gsx$place")).get("$t").toString();
	    float latitude = 0f;
	    float longitude = 0f;
	    try {
		latitude = Float.parseFloat(((JSONObject) eventEntry.get("gsx$coordinates")).get("$t").toString().split(",")[0]);
		longitude = Float.parseFloat(((JSONObject) eventEntry.get("gsx$coordinates")).get("$t").toString().split(",")[1]);
	    } catch (NumberFormatException e) {
		// the event has no coordinates
	    }
	    Event event = new Event(title, from, to, time, location, latitude, longitude);
	    events.add(event);
	}

	if (events.size() > 0)
	    Toast.makeText(getApplicationContext(), R.string.events_prompt, Toast.LENGTH_LONG).show();
	else
	    Toast.makeText(getApplicationContext(), R.string.events_prompt, Toast.LENGTH_LONG).show();
	Settings.save(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_events);

	((TextView) findViewById(R.id.eventsTitle)).setTypeface(Fonts.trocchiBold);

	if (Settings.eventsData.equals(""))
	    fetchData();
	else {
	    processJSON(Settings.eventsData);
	    populateListView();
	}
    }

    @Override
    protected Dialog onCreateDialog(int id) {
	Builder builder = new AlertDialog.Builder(this);
	switch (id) {
	    case NO_NETWORK_CONNECTION:
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setMessage(R.string.event_no_network_prompt_sync);
		builder.setTitle("No Network Connection");
		break;
	    case ERROR_SYNCING:
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setMessage(R.string.event_sync_error);
		builder.setTitle("Sync Failed");
		break;
	}
	builder.create().show();
	return super.onCreateDialog(id);
    }
}
