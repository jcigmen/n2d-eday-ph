package com.redmedia.edayph.events;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.redmedia.edayph.Fonts;
import com.redmedia.edayph.R;

public class EventDetailsScreen extends Activity {

    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_event_details);

	event = (Event) getIntent().getExtras().get("event");
	String time = !event.getTime().equals("") ? event.getTime() : "Not available";
	String place = !event.getLocation().equals("") ? event.getLocation() : "Not available";
	((TextView) findViewById(R.id.eventDetailsTitle)).setTypeface(Fonts.trocchiBold);
	((TextView) findViewById(R.id.eventDetailsTitleContent)).setTypeface(Fonts.trocchi);
	((TextView) findViewById(R.id.eventDetailsTitleContent)).setText(event.getTitle());
	((TextView) findViewById(R.id.eventDetailsFrom)).setTypeface(Fonts.trocchiBold);
	((TextView) findViewById(R.id.eventDetailsFromContent)).setTypeface(Fonts.trocchi);
	((TextView) findViewById(R.id.eventDetailsFromContent)).setText(event.getFrom());
	((TextView) findViewById(R.id.eventDetailsTo)).setTypeface(Fonts.trocchiBold);
	((TextView) findViewById(R.id.eventDetailsToContent)).setTypeface(Fonts.trocchi);
	((TextView) findViewById(R.id.eventDetailsToContent)).setText(event.getTo());
	((TextView) findViewById(R.id.eventDetailsTime)).setTypeface(Fonts.trocchiBold);
	((TextView) findViewById(R.id.eventDetailsTimeContent)).setTypeface(Fonts.trocchi);
	((TextView) findViewById(R.id.eventDetailsTimeContent)).setText(time);
	((TextView) findViewById(R.id.eventDetailsPlace)).setTypeface(Fonts.trocchiBold);
	((TextView) findViewById(R.id.eventDetailsPlaceContent)).setTypeface(Fonts.trocchi);
	((TextView) findViewById(R.id.eventDetailsPlaceContent)).setText(place);

	Toast.makeText(getApplicationContext(), "Map location for the event not available", Toast.LENGTH_LONG).show();
    }

}
