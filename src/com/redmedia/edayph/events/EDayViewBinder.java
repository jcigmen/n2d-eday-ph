package com.redmedia.edayph.events;

import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.redmedia.edayph.Fonts;

/**
 * All for the sake of changing the font of the list items in {@link ListView}
 * 
 * @author MrUseL3tter
 */
public class EDayViewBinder implements SimpleAdapter.ViewBinder {

    @Override
    public boolean setViewValue(View view, Object data, String textRepresentation) {
	final TextView textView = (TextView) view;
	textView.setTypeface(Fonts.trocchi);
	return false;
    }

}
