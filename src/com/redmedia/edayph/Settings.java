package com.redmedia.edayph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;

/**
 * @author MrUseL3tter
 */
public class Settings {

    public static boolean notificationsEnabled = true;
    public static int notificationsHour;
    public static int notificationsMinute;
    public static String eventsData = "";

    public static boolean isNetworkAvailable(Context context) {
	ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	NetworkInfo networkInfo = cm.getActiveNetworkInfo();
	// if no network is available networkInfo will be null, otherwise check if we are connected
	if (networkInfo != null && networkInfo.isConnected())
	    return true;
	return false;
    }

    public static boolean load(Context context, Resources resources) {
	// load settings
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	notificationsEnabled = prefs.getBoolean("notificationsEnabled", true);
	notificationsHour = prefs.getInt("notificationsHour", 8);
	notificationsMinute = prefs.getInt("notificationsMinute", 0);
	if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
	    return false;
	else
	    try {
		File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + ".edayph/events.json");
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuilder text = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
		    text.append(line);
		    text.append("\n");
		}
		reader.close();
		eventsData = text.toString();
	    } catch (IOException e) {
		System.out.println("There was an error loading the files!");
		return false;
	    }
	return true;
    }

    public static boolean save(Context context) {

	// save settings
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	Editor editor = prefs.edit();
	editor.putBoolean("notificationsEnabled", notificationsEnabled);
	editor.putInt("notificationsHour", notificationsHour);
	editor.putInt("notificationsMinute", notificationsMinute);
	editor.commit();
	if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
	    return false;
	else
	    try {
		new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.edayph").mkdirs();
		File file = null;
		BufferedWriter writer = null;

		// write events json
		if (!eventsData.equals("")) {
		    file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + ".edayph/events.json");
		    writer = new BufferedWriter(new FileWriter(file));
		    writer.write(eventsData);
		    writer.close();
		}
	    } catch (IOException e) {
		System.out.println("There was an error saving the files!");
		return false;
	    }
	return true;
    }
}
