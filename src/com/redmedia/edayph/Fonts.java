package com.redmedia.edayph;

import android.content.res.AssetManager;
import android.graphics.Typeface;

public class Fonts {

    public static Typeface trocchi;
    public static Typeface trocchiBold;
    public static Typeface bebas;

    public static void load(AssetManager assets) {
	trocchi = Typeface.createFromAsset(assets, "trocchi.otf");
	trocchiBold = Typeface.createFromAsset(assets, "trocchi-bold.otf");
	bebas = Typeface.createFromAsset(assets, "bebas.TTF");
    }
}
