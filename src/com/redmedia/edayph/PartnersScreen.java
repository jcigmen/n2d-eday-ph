package com.redmedia.edayph;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class PartnersScreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_partners);

	((TextView) findViewById(R.id.partnersDevelopersTitle)).setTypeface(Fonts.trocchiBold);
	((TextView) findViewById(R.id.partnersInstitutionalTitle)).setTypeface(Fonts.trocchiBold);
    }
}
