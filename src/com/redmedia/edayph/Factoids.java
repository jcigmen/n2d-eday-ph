package com.redmedia.edayph;

import java.io.IOException;
import java.io.InputStreamReader;

import android.content.res.Resources;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * @author MrUseL3tter
 */
public class Factoids {

    private static String[] tipsArray;

    public static String get() {
	return tipsArray[(int) (Math.random() * tipsArray.length)];
    }

    public static void load(Resources resources) {
	try {
	    JSONObject json = (JSONObject) JSONValue.parse(new InputStreamReader(resources.getAssets().open("factoids.json")));
	    JSONArray tipsJSONArray = (JSONArray) json.get("factoid");
	    tipsArray = new String[tipsJSONArray.size()];
	    for (int i = 0; i < tipsJSONArray.size(); i++)
		tipsArray[i] = tipsJSONArray.get(i).toString();
	} catch (IOException e) {
	}
    }
}
