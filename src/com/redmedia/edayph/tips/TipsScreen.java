package com.redmedia.edayph.tips;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.redmedia.edayph.Fonts;
import com.redmedia.edayph.R;

public class TipsScreen extends Activity implements OnClickListener {

    @Override
    public void onClick(View v) {
	setTipTexts((int) (Math.random() * TipsUtils.getTipsCount()));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_tips);

	Fonts.load(getAssets());

	((TextView) findViewById(R.id.screenTipHash)).setTypeface(Fonts.bebas);
	((TextView) findViewById(R.id.screenTipHash)).setText("#");
	((TextView) findViewById(R.id.screenTipHeader)).setTypeface(Fonts.bebas);
	((TextView) findViewById(R.id.screenTipHeader)).setText("TIP");
	((TextView) findViewById(R.id.screenTipNumber)).setTypeface(Fonts.bebas);
	((TextView) findViewById(R.id.screenTipNumber)).setText("89");
	((TextView) findViewById(R.id.screenTipCategory)).setTypeface(Fonts.trocchiBold);
	((TextView) findViewById(R.id.screenTipsText)).setTypeface(Fonts.trocchi);
	((TextView) findViewById(R.id.screenTipCategory)).setText("Sustainable Lifestyle");

	((ImageView) findViewById(R.id.screenTipsButton1)).setOnClickListener(this);

	// this activity was not called by NotificationBroadcastReceiver
	if (getIntent().getIntExtra("position", -1) == -1)
	    setTipTexts((int) (Math.random() * TipsUtils.getTipsCount()));
	else
	    setTipTexts(getIntent().getIntExtra("position", 0));
    }

    private void setTipTexts(int id) {
	Tip tip = TipsUtils.get(id);
	switch (tip.getId()) {
	    case Tip.ADVOCACY:
		((TextView) findViewById(R.id.screenTipCategory)).setText("Advocacy");
		break;
	    case Tip.GREEN_ENERGY:
		((TextView) findViewById(R.id.screenTipCategory)).setText("Green Energy");
		break;
	    case Tip.SUSTAINABLE_LIFESTYLE:
		((TextView) findViewById(R.id.screenTipCategory)).setText("Sustainable Lifestyle");
		break;
	    case Tip.WATER_CONSERVATION:
		((TextView) findViewById(R.id.screenTipCategory)).setText("Water Conservation");
		break;
	}
	((TextView) findViewById(R.id.screenTipsText)).setText(tip.getMessage());
	String tipNumber = id + 1 > 8 ? "" + (id + 1) : "0" + (id + 1);
	((TextView) findViewById(R.id.screenTipNumber)).setText(tipNumber);
	((ScrollView) findViewById(R.id.scrollView1)).scrollTo(0, 0);
    }
}
