package com.redmedia.edayph.tips;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.res.Resources;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class TipsUtils {

    private static ArrayList<Tip> tips = new ArrayList<Tip>();

    public static Tip get(int position) {
	if (position < tips.size())
	    return tips.get(position);
	else
	    return tips.get((int) (Math.random() * tips.size()));
    }

    public static Tip getRandom() {
	return tips.get((int) (Math.random() * tips.size()));
    }

    public static int getTipsCount() {
	return tips.size();
    }

    public static void load(Resources resources) {
	tips = new ArrayList<Tip>();
	loadAdvocacy(resources);
	loadGreenEnergy(resources);
	loadSustainableLifestyle(resources);
	loadWaterConservation(resources);
    }

    private static void loadAdvocacy(Resources resources) {
	try {
	    JSONObject json = (JSONObject) JSONValue.parse(new InputStreamReader(resources.getAssets().open("tips_advocacy.json")));
	    JSONArray tipsJSONArray = (JSONArray) json.get("tips");
	    for (int i = 0; i < tipsJSONArray.size(); i++)
		tips.add(new Tip(Tip.ADVOCACY, tipsJSONArray.get(i).toString()));
	} catch (IOException e) {
	}
    }

    private static void loadGreenEnergy(Resources resources) {
	try {
	    JSONObject json = (JSONObject) JSONValue.parse(new InputStreamReader(resources.getAssets().open("tips_green_energy.json")));
	    JSONArray tipsJSONArray = (JSONArray) json.get("tips");
	    for (int i = 0; i < tipsJSONArray.size(); i++)
		tips.add(new Tip(Tip.GREEN_ENERGY, tipsJSONArray.get(i).toString()));
	} catch (IOException e) {
	}
    }

    private static void loadSustainableLifestyle(Resources resources) {
	try {
	    JSONObject json = (JSONObject) JSONValue.parse(new InputStreamReader(resources.getAssets().open("tips_sustainable.json")));
	    JSONArray tipsJSONArray = (JSONArray) json.get("tips");
	    for (int i = 0; i < tipsJSONArray.size(); i++)
		tips.add(new Tip(Tip.SUSTAINABLE_LIFESTYLE, tipsJSONArray.get(i).toString()));
	} catch (IOException e) {
	}
    }

    private static void loadWaterConservation(Resources resources) {
	try {
	    JSONObject json = (JSONObject) JSONValue.parse(new InputStreamReader(resources.getAssets().open("tips_water_conservation.json")));
	    JSONArray tipsJSONArray = (JSONArray) json.get("tips");
	    for (int i = 0; i < tipsJSONArray.size(); i++)
		tips.add(new Tip(Tip.WATER_CONSERVATION, tipsJSONArray.get(i).toString()));
	} catch (IOException e) {
	}
    }
}
