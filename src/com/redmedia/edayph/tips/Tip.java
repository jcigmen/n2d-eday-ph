package com.redmedia.edayph.tips;

/**
 * @author MrUseL3tter
 */
public class Tip {

    public static final int GREEN_ENERGY = 0;
    public static final int SUSTAINABLE_LIFESTYLE = 1;
    public static final int WATER_CONSERVATION = 2;
    public static final int ADVOCACY = 3;

    private final int id;

    private final String message;

    public Tip(int id, String message) {
	this.id = id;
	this.message = message;
    }

    /**
     * @return the id
     */
    public int getId() {
	return id;
    }

    /**
     * @return the message
     */
    public String getMessage() {
	return message;
    }

}
