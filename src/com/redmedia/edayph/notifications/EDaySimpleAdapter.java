package com.redmedia.edayph.notifications;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.redmedia.edayph.Fonts;
import com.redmedia.edayph.R;
import com.redmedia.edayph.Settings;

public class EDaySimpleAdapter extends SimpleAdapter {

    private final Context context;
    private CheckBox checkBox;

    public EDaySimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
	super(context, data, resource, from, to);
	this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	if (convertView == null) {
	    System.out.println("convertView not null");
	    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    convertView = inflater.inflate(R.layout.screen_notification_list_format, parent, false);
	    if (checkBox == null)
		checkBox = (CheckBox) convertView.findViewById(R.id.checkBox1);
	    TextView title = (TextView) convertView.findViewById(R.id.notificationText);
	    title.setTypeface(Fonts.trocchi);
	    TextView subText = (TextView) convertView.findViewById(R.id.notificationSubtext);
	    subText.setTypeface(Fonts.trocchi);
	}
	checkBox.setChecked(Settings.notificationsEnabled);
	return convertView;
    }

    public void setCheckBoxStatus(boolean checked) {
	System.out.println("setCheckBoxStatus(boolean checked)");
	checkBox.setChecked(checked);
    }
}
