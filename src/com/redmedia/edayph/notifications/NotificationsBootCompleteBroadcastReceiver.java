package com.redmedia.edayph.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.redmedia.edayph.Settings;

public class NotificationsBootCompleteBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent i) {
	// repeat the alarm in case this receiver was called due to a boot completion
	Settings.load(context, context.getResources());
	if (Settings.notificationsEnabled)
	    NotificationsUtils.setAlarm(context);
    }

}
