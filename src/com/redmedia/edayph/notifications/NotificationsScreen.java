package com.redmedia.edayph.notifications;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TimePicker;
import android.widget.Toast;

import com.redmedia.edayph.R;
import com.redmedia.edayph.Settings;
import com.redmedia.edayph.events.EDayViewBinder;

public class NotificationsScreen extends ListActivity implements OnItemClickListener, OnTimeSetListener {

    private EDaySimpleAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_notification);

	ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
	HashMap<String, String> map = new HashMap<String, String>();
	map.put("title", getResources().getString(R.string.notifications_settings));
	map.put("subtext", getResources().getString(R.string.notifications_subtext));
	list.add(map);
	adapter = new EDaySimpleAdapter(getApplicationContext(), list, R.layout.screen_notification_list_format, new String[] { "title", "subtext" }, new int[] { R.id.notificationText, R.id.notificationSubtext });
	adapter.setViewBinder(new EDayViewBinder());
	setListAdapter(adapter);
	getListView().setOnItemClickListener(this);

	if (Settings.notificationsEnabled)
	    showNotificiationPrompt();
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	if (!Settings.notificationsEnabled)
	    showDialog(0);
	else {
	    Settings.notificationsEnabled = !Settings.notificationsEnabled;
	    adapter.setCheckBoxStatus(Settings.notificationsEnabled);
	    NotificationsUtils.cancelAlarm(getApplicationContext());
	}
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
	Settings.notificationsEnabled = true;
	Settings.notificationsHour = hourOfDay;
	Settings.notificationsMinute = minute;
	Settings.save(getApplicationContext());
	NotificationsUtils.setAlarm(getApplicationContext());
	adapter.setCheckBoxStatus(Settings.notificationsEnabled);
	showNotificiationPrompt();
    }

    private void showNotificiationPrompt() {
	String minute = Settings.notificationsMinute < 10 ? "0" + Settings.notificationsMinute : "" + Settings.notificationsMinute;
	String hour = Settings.notificationsHour == 0 ? "12" : Settings.notificationsHour < 10 ? "0" + Settings.notificationsHour : Settings.notificationsHour < 13 ? Settings.notificationsHour + "" : Settings.notificationsHour - 12 + "";
	Toast.makeText(getApplicationContext(), "Notification will popup every " + hour + ":" + minute + (Settings.notificationsHour < 13 ? " AM" : " PM"), Toast.LENGTH_LONG).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	TimePickerDialog dialog = new TimePickerDialog(this, this, prefs.getInt("notificationsHour", 8), prefs.getInt("notificationsMinute", 0), false);
	return dialog;
    }
}
