package com.redmedia.edayph.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.redmedia.edayph.R;
import com.redmedia.edayph.tips.TipsScreen;

/**
 * author MrUseL3tter
 */
public class NotificationsBroadcastReceiver extends BroadcastReceiver {

    public static final int NOTIFICATION = 311583407;

    @Override
    public void onReceive(Context context, Intent i) {
	NotificationManager notificationsManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	Notification notification = new Notification(R.drawable.app_icon, "Daily Green Tips!", System.currentTimeMillis());
	Intent intent = new Intent(context, TipsScreen.class);
	PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
	notification.setLatestEventInfo(context, "Daily Green Tips!", "Tap here for your daily green tip", contentIntent);
	notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL; // removes the notification on click
	notificationsManager.notify(NOTIFICATION, notification);
    }

}
