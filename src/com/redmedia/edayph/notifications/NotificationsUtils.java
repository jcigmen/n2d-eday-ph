package com.redmedia.edayph.notifications;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.redmedia.edayph.Settings;

/**
 * @author MrUseL3tter
 */
public class NotificationsUtils {

    public static void cancelAlarm(Context context) {
	Intent intent = new Intent(context, NotificationsBroadcastReceiver.class);
	PendingIntent pendingIntent = PendingIntent.getBroadcast(context, NotificationsBroadcastReceiver.NOTIFICATION, intent, PendingIntent.FLAG_CANCEL_CURRENT);
	AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	alarmManager.cancel(pendingIntent);
    }

    public static void setAlarm(Context context) {
	cancelAlarm(context);
	Settings.load(context, context.getResources());
	Calendar calendar = Calendar.getInstance();
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.MINUTE, Settings.notificationsMinute);
	calendar.set(Calendar.HOUR_OF_DAY, Settings.notificationsHour);
	calendar.set(Calendar.AM_PM, Settings.notificationsHour < 13 ? Calendar.AM : Calendar.PM);
	//	System.out.println(calendar.get(Calendar.HOUR_OF_DAY) + "   " + calendar.get(Calendar.MINUTE));
	System.out.println("calendar.getTimeInMillis(): " + calendar.getTimeInMillis());
	System.out.println("System.currentTimeMillis(): " + System.currentTimeMillis());
	System.out.println("From settings: " + Settings.notificationsHour + "   " + Settings.notificationsMinute);
	Intent intent = new Intent(context, NotificationsBroadcastReceiver.class);
	PendingIntent pendingIntent = PendingIntent.getBroadcast(context, NotificationsBroadcastReceiver.NOTIFICATION, intent, PendingIntent.FLAG_CANCEL_CURRENT);
	AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() + (calendar.getTimeInMillis() < System.currentTimeMillis() ? AlarmManager.INTERVAL_DAY : 0), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}
