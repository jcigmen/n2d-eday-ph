package com.redmedia.edayph;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.redmedia.edayph.carboncalc.CarbonAgentQuestions;
import com.redmedia.edayph.carboncalc.CarbonFootprintScreen;
import com.redmedia.edayph.events.Event;
import com.redmedia.edayph.events.EventDetailsScreen;
import com.redmedia.edayph.events.EventsMapScreen;
import com.redmedia.edayph.events.EventsScreen;
import com.redmedia.edayph.notifications.NotificationsScreen;
import com.redmedia.edayph.notifications.NotificationsUtils;
import com.redmedia.edayph.tips.TipsScreen;

public class N2DEdayMain extends FragmentActivity implements OnTouchListener {

    private ArrayList<Event> eventsList;
    private ImageView tag;
    private ImageView tips;
    private ImageView events;
    private ImageView donate;
    private ImageView calculator;
    private ImageView news;
    private ImageView partners;
    private TextView newsText;
    private int eventIndex = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_home);

	Fonts.load(getAssets());
	Settings.load(getApplicationContext(), getResources());
	Factoids.load(getResources());
	CarbonAgentQuestions.load(getResources());

	tag = (ImageView) findViewById(R.id.homeTag);
	tag.setOnTouchListener(this);

	tips = (ImageView) findViewById(R.id.homeTips);
	tips.setOnTouchListener(this);

	events = (ImageView) findViewById(R.id.homeEvents);
	events.setOnTouchListener(this);

	calculator = (ImageView) findViewById(R.id.homeCalc);
	calculator.setOnTouchListener(this);

	donate = (ImageView) findViewById(R.id.homeDonate);
	donate.setOnTouchListener(this);

	news = (ImageView) findViewById(R.id.homeNews);
	news.setOnTouchListener(this);

	partners = (ImageView) findViewById(R.id.homePartners);
	partners.setOnTouchListener(this);

	newsText = (TextView) findViewById(R.id.homeNewsText);
	newsText.setTypeface(Fonts.trocchi);
	newsText.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    if (v.getId() != R.id.homeNewsText)
		((ImageView) v).setColorFilter(0x46464646, PorterDuff.Mode.MULTIPLY);
	    switch (v.getId()) {
		case R.id.homeTag:
		    startActivity(new Intent(getApplicationContext(), NotificationsScreen.class));
		    break;
		case R.id.homeCalc:
		    startActivity(new Intent(getApplicationContext(), CarbonFootprintScreen.class));
		    break;
		case R.id.homeDonate:
		    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.earthdayphilippines.org/")));
		    break;
		case R.id.homeEvents:
		    startActivity(new Intent(getApplicationContext(), EventsScreen.class));
		    break;
		case R.id.homePartners:
		    startActivity(new Intent(getApplicationContext(), PartnersScreen.class));
		    break;
		case R.id.homeTips:
		    startActivity(new Intent(getApplicationContext(), TipsScreen.class));
		    break;
		case R.id.homeNews:
		    newsText.setTextColor(0x46464646);
		    newsClicked();
		    break;
		case R.id.homeNewsText:
		    newsText.setTextColor(0x46464646);
		    newsClicked();
		    break;
	    }
	} else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_MOVE)
	    if (v.getId() != R.id.homeNewsText)
		((ImageView) v).clearColorFilter();
	    else
		newsText.setTextColor(Color.WHITE);
	return true;
    }

    private void newsClicked() {
	if (eventsList.size() > 0) {
	    Intent intent = null;
	    if (!eventsList.get(eventIndex).hasMapLocation()) {
		intent = new Intent(getApplicationContext(), EventDetailsScreen.class);
		intent.putExtra("event", eventsList.get(eventIndex));
		startActivity(intent);
	    } else {
		intent = new Intent(getApplicationContext(), EventsMapScreen.class);
		intent.putExtra("title", eventsList.get(eventIndex).getTitle());
		intent.putExtra("to", eventsList.get(eventIndex).getTo());
		intent.putExtra("from", eventsList.get(eventIndex).getFrom());
		intent.putExtra("time", eventsList.get(eventIndex).getTime());
		intent.putExtra("latitude", new float[] { eventsList.get(eventIndex).getLatitude(), eventsList.get(eventIndex).getLongitude() });
		startActivity(intent);
	    }
	}
    }

    private void processJSON(String raw) {
	JSONObject parent = (JSONObject) JSONValue.parse(raw);
	JSONObject feed = (JSONObject) parent.get("feed");
	JSONArray entries = (JSONArray) feed.get("entry");
	for (int i = 0; i < entries.size(); i++) {
	    JSONObject eventEntry = (JSONObject) entries.get(i);
	    String title = ((JSONObject) eventEntry.get("gsx$eventtitle")).get("$t").toString();
	    String from = ((JSONObject) eventEntry.get("gsx$datefrom")).get("$t").toString();
	    String to = ((JSONObject) eventEntry.get("gsx$dateto")).get("$t").toString();
	    String time = ((JSONObject) eventEntry.get("gsx$time")).get("$t").toString();
	    String location = ((JSONObject) eventEntry.get("gsx$place")).get("$t").toString();
	    float latitude = 0f;
	    float longitude = 0f;
	    try {
		latitude = Float.parseFloat(((JSONObject) eventEntry.get("gsx$coordinates")).get("$t").toString().split(",")[0]);
		longitude = Float.parseFloat(((JSONObject) eventEntry.get("gsx$coordinates")).get("$t").toString().split(",")[1]);
	    } catch (NumberFormatException e) {
		// the event has no coordinates
	    }
	    Event event = new Event(title, from, to, time, location, latitude, longitude);
	    eventsList.add(event);
	}
    }

    private void runNewsThread() {
	final Handler handler = new Handler();
	handler.post(new Thread() {

	    @Override
	    public void run() {
		eventIndex = eventIndex < eventsList.size() - 1 ? eventIndex + 1 : 0;
		handler.postDelayed(this, 4000);
		newsText.setText(eventsList.get(eventIndex).getTitle());
	    }

	});
    }

    /** restart the notification */
    @Override
    protected void onResume() {
	super.onResume();

	eventsList = new ArrayList<Event>();
	if (!Settings.eventsData.equals("")) {
	    processJSON(Settings.eventsData);
	    runNewsThread();
	}

	if (Settings.notificationsEnabled)
	    NotificationsUtils.setAlarm(getApplicationContext());
    }
}
