package com.redmedia.edayph.carboncalc;

import java.io.IOException;
import java.io.InputStreamReader;

import android.content.res.Resources;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Loads the json file for the questions for each carbon agent and makes it available as a String.
 * 
 * @author MrUseL3tter
 */
public class CarbonAgentQuestions {

    private static String[] questions;

    /**
     * @param id any of the static final agent variables from {@link CarbonFootprintScreen}
     */
    public static String get(int id) {
	switch (id) {
	    case CarbonFootprintScreen.BICYCLE:
		return questions[CarbonFootprintScreen.BICYCLE];
	    case CarbonFootprintScreen.CAR:
		return questions[CarbonFootprintScreen.CAR];
	    case CarbonFootprintScreen.JEEPNEY:
		return questions[CarbonFootprintScreen.JEEPNEY];
	    case CarbonFootprintScreen.LIGHT_BULB:
		return questions[CarbonFootprintScreen.LIGHT_BULB];
	    case CarbonFootprintScreen.BOTTLED_WATER:
		return questions[CarbonFootprintScreen.BOTTLED_WATER];
	    case CarbonFootprintScreen.PAPER:
		return questions[CarbonFootprintScreen.PAPER];
	    case CarbonFootprintScreen.PLASTIC_BAG:
		return questions[CarbonFootprintScreen.PLASTIC_BAG];
	    case CarbonFootprintScreen.AIR_CONDITIONER:
		return questions[CarbonFootprintScreen.AIR_CONDITIONER];
	    case CarbonFootprintScreen.AIRPLANE:
		return questions[CarbonFootprintScreen.AIRPLANE];
	    case CarbonFootprintScreen.MOBILE_PHONE:
		return questions[CarbonFootprintScreen.MOBILE_PHONE];
	    default:
		return "";
	}
    }

    public static void load(Resources resources) {
	try {
	    JSONObject json = (JSONObject) JSONValue.parse(new InputStreamReader(resources.getAssets().open("questions.json")));
	    JSONArray tipsJSONArray = (JSONArray) json.get("questions");
	    questions = new String[tipsJSONArray.size()];
	    for (int i = 0; i < tipsJSONArray.size(); i++)
		questions[i] = tipsJSONArray.get(i).toString();
	} catch (IOException e) {
	}
    }
}
