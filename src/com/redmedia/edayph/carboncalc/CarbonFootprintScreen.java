package com.redmedia.edayph.carboncalc;

import java.util.ArrayList;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.noobs2d.brochure.viewpager.ViewPagerAdapter;
import com.redmedia.edayph.Factoids;
import com.redmedia.edayph.Fonts;
import com.redmedia.edayph.R;
import com.redmedia.edayph.viewpager.EDayViewPagerAdapter;
import com.redmedia.edayph.viewpager.fragments.AirConditionerFragment;
import com.redmedia.edayph.viewpager.fragments.AirplaneFragment;
import com.redmedia.edayph.viewpager.fragments.BicycleFragment;
import com.redmedia.edayph.viewpager.fragments.BottledWaterFragment;
import com.redmedia.edayph.viewpager.fragments.CarFragment;
import com.redmedia.edayph.viewpager.fragments.JeepneyFragment;
import com.redmedia.edayph.viewpager.fragments.LightBulbFragment;
import com.redmedia.edayph.viewpager.fragments.MobilePhoneFragment;
import com.redmedia.edayph.viewpager.fragments.PaperFragment;
import com.redmedia.edayph.viewpager.fragments.PlasticBagFragment;

/**
 * Screen with flippable images of carbon agents, with an adjustable seekbar representing each
 * agent's carbon footprint based on the input variable.
 * 
 * @author MrUseL3tter
 */
public class CarbonFootprintScreen extends FragmentActivity implements OnClickListener, OnPageChangeListener, OnSeekBarChangeListener {

    public static final int BICYCLE = 0;
    public static final int CAR = 1;
    public static final int JEEPNEY = 2;
    public static final int LIGHT_BULB = 3;
    public static final int BOTTLED_WATER = 4;
    public static final int PAPER = 5;
    public static final int PLASTIC_BAG = 6;
    public static final int AIR_CONDITIONER = 7;
    public static final int AIRPLANE = 8;
    public static final int MOBILE_PHONE = 9;

    private ArrayList<ImageView> leaves;
    private CarbonAgent bicycleAgent;
    private CarbonAgent carAgent;
    private CarbonAgent jeepneyAgent;
    private CarbonAgent lightBulbAgent;
    private CarbonAgent bottledWaterAgent;
    private CarbonAgent paperAgent;
    private CarbonAgent plasticBagAgent;
    private CarbonAgent airConditionerAgent;
    private CarbonAgent airplaneAgent;
    private CarbonAgent mobilePhoneAgent;
    private ViewPager viewPager;
    private SeekBar seekBar;
    private TextView carbonFootprintValue;
    private TextView inputBarValue;
    private TextView tipText;
    private TextView tipHeader;
    private TextView question;

    @Override
    public void onClick(View v) {
	hideTipText();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_carbon_footprint_calc);

	bicycleAgent = new CarbonAgent(getResources(), "Bicycle");
	carAgent = new CarbonAgent(getResources(), "Car");
	jeepneyAgent = new CarbonAgent(getResources(), "Jeepney");
	lightBulbAgent = new CarbonAgent(getResources(), "Light Bulb");
	bottledWaterAgent = new CarbonAgent(getResources(), "Bottled Water");
	paperAgent = new CarbonAgent(getResources(), "Paper");
	plasticBagAgent = new CarbonAgent(getResources(), "Plastic Bag");
	airConditionerAgent = new CarbonAgent(getResources(), "Air Conditioner");
	airplaneAgent = new CarbonAgent(getResources(), "Airplane");
	mobilePhoneAgent = new CarbonAgent(getResources(), "Mobile Phone");

	seekBar = (SeekBar) findViewById(R.id.seekBar);
	seekBar.setOnSeekBarChangeListener(this);

	ViewPagerAdapter adapter = new EDayViewPagerAdapter(getSupportFragmentManager());
	adapter.addFragment(new BicycleFragment());
	adapter.addFragment(new CarFragment());
	adapter.addFragment(new JeepneyFragment());
	adapter.addFragment(new LightBulbFragment());
	adapter.addFragment(new BottledWaterFragment());
	adapter.addFragment(new PaperFragment());
	adapter.addFragment(new PlasticBagFragment());
	adapter.addFragment(new AirConditionerFragment());
	adapter.addFragment(new AirplaneFragment());
	adapter.addFragment(new MobilePhoneFragment());
	viewPager = (ViewPager) findViewById(R.id.pager);
	viewPager.setAdapter(adapter);
	int offScreenPages = 9;
	viewPager.setOffscreenPageLimit(offScreenPages);
	viewPager.setOnPageChangeListener(this);

	tipText = (TextView) findViewById(R.id.tipText);
	tipText.setText("");
	tipText.setVisibility(View.INVISIBLE);
	tipText.setTextColor(0xFF2f5875);
	tipText.setTypeface(Fonts.trocchi);
	tipText.setOnClickListener(this);

	tipHeader = (TextView) findViewById(R.id.tipHeader);
	tipHeader.setVisibility(View.INVISIBLE);
	tipHeader.setTypeface(Fonts.trocchiBold);
	tipHeader.setTextColor(Color.WHITE);

	question = (TextView) findViewById(R.id.questionText);
	question.setTypeface(Fonts.trocchiBold);
	question.setTextColor(0xFF2f5875);
	question.setShadowLayer(2, 1, 1, Color.WHITE);
	question.setText(CarbonAgentQuestions.get(BICYCLE));

	carbonFootprintValue = (TextView) findViewById(R.id.carbonFootPrintText);
	carbonFootprintValue.setTypeface(Fonts.trocchiBold);
	carbonFootprintValue.setTextColor(0xFF97be56);
	carbonFootprintValue.setShadowLayer(1, 1, 1, Color.GRAY);
	inputBarValue = (TextView) findViewById(R.id.inputValueText);
	inputBarValue.setTextColor(0xFF2f5875);
	inputBarValue.setShadowLayer(1, 1, 1, Color.GRAY);
	inputBarValue.setTypeface(Fonts.trocchi);
	setValueTexts(seekBar.getProgress());

	leaves = new ArrayList<ImageView>();
	leaves.add((ImageView) findViewById(R.id.leaf1));
	leaves.add((ImageView) findViewById(R.id.leaf2));
	leaves.add((ImageView) findViewById(R.id.leaf3));
	leaves.add((ImageView) findViewById(R.id.leaf4));
	leaves.get(1).setAlpha(0);
	leaves.get(1).setVisibility(View.INVISIBLE);
	leaves.get(2).setAlpha(0);
	leaves.get(2).setVisibility(View.INVISIBLE);
	leaves.get(3).setAlpha(0);
	leaves.get(3).setVisibility(View.INVISIBLE);

	transition();
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
	transition();
	setValueTexts(seekBar.getProgress());
	hideTipText();
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
	transition();
	hideTipText();
    }

    @Override
    public void onPageSelected(int arg0) {
	transition();
	hideTipText();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
	hideTipText();
	transition();
	setValueTexts(progress);
	if (progress == seekBar.getMax())
	    setTipText();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    private void hideTipText() {
	if (tipText.getVisibility() == View.VISIBLE) {
	    tipText.clearAnimation();
	    tipText.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
	    tipText.setVisibility(View.INVISIBLE);
	    tipHeader.clearAnimation();
	    tipHeader.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
	    tipHeader.setVisibility(View.INVISIBLE);
	}
    }

    private void setTipText() {
	tipText.clearAnimation();
	tipText.setText(Factoids.get());
	tipText.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in));
	tipText.setVisibility(View.VISIBLE);
	tipHeader.clearAnimation();
	tipHeader.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in));
	tipHeader.setVisibility(View.VISIBLE);
    }

    private void setValueTexts(int progress) {
	switch (viewPager.getCurrentItem()) {
	    case BICYCLE:
		inputBarValue.setText(bicycleAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(bicycleAgent.getFootPrintValue(seekBar.getProgress()) + " " + bicycleAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(BICYCLE)));
		break;
	    case CAR:
		inputBarValue.setText(carAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(carAgent.getFootPrintValue(seekBar.getProgress()) + " " + carAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(CAR)));
		break;
	    case JEEPNEY:
		inputBarValue.setText(jeepneyAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(jeepneyAgent.getFootPrintValue(seekBar.getProgress()) + " " + jeepneyAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(JEEPNEY)));
		break;
	    case LIGHT_BULB:
		inputBarValue.setText(lightBulbAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(lightBulbAgent.getFootPrintValue(seekBar.getProgress()) + " " + lightBulbAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(LIGHT_BULB)));
		break;
	    case BOTTLED_WATER:
		inputBarValue.setText(bottledWaterAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(bottledWaterAgent.getFootPrintValue(seekBar.getProgress()) + " " + bottledWaterAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(BOTTLED_WATER)));
		break;
	    case PAPER:
		inputBarValue.setText(paperAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(paperAgent.getFootPrintValue(seekBar.getProgress()) + " " + paperAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(PAPER)));
		break;
	    case PLASTIC_BAG:
		inputBarValue.setText(plasticBagAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(plasticBagAgent.getFootPrintValue(seekBar.getProgress()) + " " + plasticBagAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(PLASTIC_BAG)));
		break;
	    case AIR_CONDITIONER:
		inputBarValue.setText(airConditionerAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(airConditionerAgent.getFootPrintValue(seekBar.getProgress()) + " " + airConditionerAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(AIR_CONDITIONER)));
		break;
	    case AIRPLANE:
		inputBarValue.setText(airplaneAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(airplaneAgent.getFootPrintValue(seekBar.getProgress()) + " " + airplaneAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(AIRPLANE)));
		break;
	    case MOBILE_PHONE:
		inputBarValue.setText(mobilePhoneAgent.getInputValue(seekBar.getProgress()));
		carbonFootprintValue.setText(Html.fromHtml(mobilePhoneAgent.getFootPrintValue(seekBar.getProgress()) + " " + mobilePhoneAgent.getFootPrintSuffix()));
		question.setText(Html.fromHtml(CarbonAgentQuestions.get(MOBILE_PHONE)));
		break;
	}
    }

    private void transition() {
	int progress = seekBar.getProgress();
	// the current view is the bike, seekbar value doesn't matter
	if (viewPager.getCurrentItem() == 0) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(0).clearAnimation();
	    leaves.get(0).setAlpha(1000);
	    leaves.get(0).setVisibility(View.VISIBLE);
	}
	// the current view is the car/jeep/lightbulb/bottledwater, leaf1 (green) is not visible and seekbar progress is 0-3
	else if (viewPager.getCurrentItem() >= 1 && viewPager.getCurrentItem() <= 4 && progress > 0 && progress < 4 && leaves.get(0).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(0).clearAnimation();
	    leaves.get(0).setAlpha(1000);
	    leaves.get(0).setVisibility(View.VISIBLE);
	}
	// the current view is the car/jeep/lightbulb/bottledwater, leaf2 is not visible and seekbar progress is 4-5
	else if (viewPager.getCurrentItem() >= 1 && viewPager.getCurrentItem() <= 4 && (progress == 4 || progress == 5) && leaves.get(1).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(1).clearAnimation();
	    leaves.get(1).setAlpha(1000);
	    leaves.get(1).setVisibility(View.VISIBLE);
	}
	// the current view is the car/jeep/lightbulb/bottledwater, leaf3 is not visible and seekbar progress is 6
	else if (viewPager.getCurrentItem() >= 1 && viewPager.getCurrentItem() <= 4 && progress == 6 && leaves.get(2).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(2).clearAnimation();
	    leaves.get(2).setAlpha(1000);
	    leaves.get(2).setVisibility(View.VISIBLE);
	}
	// the current view is the car/jeep/lightbulb/bottledwater, leaf4 is not visible and seekbar progress is 7
	else if (viewPager.getCurrentItem() >= 1 && viewPager.getCurrentItem() <= 4 && progress == 7 && leaves.get(3).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(3).clearAnimation();
	    leaves.get(3).setAlpha(1000);
	    leaves.get(3).setVisibility(View.VISIBLE);
	}
	// the current view is the paper/plasticbag/aircon/airplane/mobilephone, leaf1 is not visible and seekbar progress is less than 0-2
	else if (viewPager.getCurrentItem() > 4 && progress <= 2 && leaves.get(0).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(0).clearAnimation();
	    leaves.get(0).setAlpha(1000);
	    leaves.get(0).setVisibility(View.VISIBLE);
	}
	// the current view is the paper/plasticbag/aircon/airplane/mobilephone, leaf2 is not visible and seekbar progress is exactly 3
	else if (viewPager.getCurrentItem() > 4 && progress == 3 && leaves.get(1).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(1).clearAnimation();
	    leaves.get(1).setAlpha(1000);
	    leaves.get(1).setVisibility(View.VISIBLE);
	}
	// the current view is the paper/plasticbag/aircon/airplane/mobilephone, leaf3 is not visible and seekbar progress is 4 or 5
	else if (viewPager.getCurrentItem() > 4 && (progress == 4 || progress == 5) && leaves.get(2).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(2).clearAnimation();
	    leaves.get(2).setAlpha(1000);
	    leaves.get(2).setVisibility(View.VISIBLE);
	}
	// the current view is the paper/plasticbag/aircon/airplane/mobilephone, leaf1 is not visible and seekbar progress is 6 or 7
	else if (viewPager.getCurrentItem() > 4 && (progress == 6 || progress == 9) && leaves.get(3).getVisibility() == View.INVISIBLE) {
	    for (int i = 0; i < leaves.size(); i++)
		if (leaves.get(i).getVisibility() == View.VISIBLE) {
		    leaves.get(i).clearAnimation();
		    leaves.get(i).setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
		    leaves.get(i).setVisibility(View.INVISIBLE);
		}
	    leaves.get(3).clearAnimation();
	    leaves.get(3).setAlpha(1000);
	    leaves.get(3).setVisibility(View.VISIBLE);
	}
    }

}
