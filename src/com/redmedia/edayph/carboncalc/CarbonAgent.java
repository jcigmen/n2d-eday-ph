package com.redmedia.edayph.carboncalc;

import java.io.IOException;
import java.io.InputStreamReader;

import android.content.res.Resources;
import android.widget.SeekBar;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Representation of an agent from which a carbon footprint can be computed from. An agent has 8
 * input units, each matching a footprint value.
 * 
 * @author MrUseL3tter
 */
public class CarbonAgent {

    private final String name;
    private final String footprintSuffix;
    private final String[] inputValues;
    private final String[] footprintValues;

    public CarbonAgent(Resources resources, String name) {
	this.name = name;
	JSONObject parentJSON = null;
	try {
	    parentJSON = (JSONObject) JSONValue.parse(new InputStreamReader(resources.getAssets().open("carbonfootprint.json")));
	} catch (IOException e) {
	}
	JSONObject agentObject = (JSONObject) parentJSON.get(name);
	footprintSuffix = agentObject.get("footprintSuffix").toString();
	JSONArray inputValuesArray = (JSONArray) agentObject.get("inputValues");
	inputValues = new String[8];
	for (int i = 0; i < inputValuesArray.size(); i++)
	    inputValues[i] = inputValuesArray.get(i).toString();
	JSONArray footprintValuesArray = (JSONArray) agentObject.get("footprintValues");
	footprintValues = new String[8];
	for (int i = 0; i < footprintValuesArray.size(); i++)
	    footprintValues[i] = footprintValuesArray.get(i).toString();
    }

    /**
     * @return the footprintSuffix
     */
    public String getFootPrintSuffix() {
	return footprintSuffix;
    }

    /**
     * @param seekBarProgress {@link SeekBar#getProgress()} 0-7
     * @return the footprint for that specific input value as an index from the {@link SeekBar}
     */
    public String getFootPrintValue(int seekBarProgress) {
	return footprintValues[seekBarProgress];
    }

    /**
     * @return the inputValues
     */
    public String getInputValue(int seekBarProgress) {
	return inputValues[seekBarProgress];
    }

    /**
     * @return the name
     */
    public String getName() {
	return name;
    }
}
