package com.redmedia.edayph.viewpager.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.noobs2d.brochure.viewpager.ViewPagerFragment;
import com.redmedia.edayph.R;

public class JeepneyFragment extends ViewPagerFragment {

    ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	if (rootView == null)
	    rootView = (ViewGroup) inflater.inflate(R.layout.fragment_jeepney, container, false);
	return rootView;
    }
}
