package com.redmedia.edayph.viewpager;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.noobs2d.brochure.viewpager.ViewPagerAdapter;
import com.noobs2d.brochure.viewpager.ViewPagerFragment;

public class EDayViewPagerAdapter extends ViewPagerAdapter {

    public EDayViewPagerAdapter(ArrayList<ViewPagerFragment> viewPageFragments, FragmentManager fragmentManager) {
	super(viewPageFragments, fragmentManager);
    }

    public EDayViewPagerAdapter(FragmentManager fragmentManager) {
	super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
	return getViewPageFragments().get(position);
    }
}
